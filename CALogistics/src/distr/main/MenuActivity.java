package distr.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import ca.apps.logistics.R;
import distr.common.backPressCloseHandler;
import distr.common.commons;
import distr.course.CourseAlarm;
import distr.course.CourseList;
import distr.user.UserLogin;
import distr.user.UserPassword;

import java.util.ArrayList;
import java.util.HashMap;

public class MenuActivity extends Activity {

	private backPressCloseHandler backPress;

	Button home; // title
	Button todayCourseBtn; // 오늘배차
	Button logOutBtn; // 로그아웃
	Button courseAlarmBtn; // 배차푸시
	Button passChangeBtn; // 비밀번호 변경
	String carId; // 사용자 아이디

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);
		// 버튼 선언
		home = (Button) findViewById(R.id.home);
		todayCourseBtn = (Button) findViewById(R.id.todayCourse); // 오늘 배차 버튼
		logOutBtn = (Button) findViewById(R.id.logOut); // 로그아웃 버튼
		courseAlarmBtn = (Button) findViewById(R.id.courseAlarm); // 배차푸시 버튼
		passChangeBtn = (Button) findViewById(R.id.passChange); // 비밀번호변경 버튼

		backPress = new backPressCloseHandler(this);

		// 로그인 사용자 데이터 가져오기
		Intent intent = getIntent();

		final HashMap<String, String> user = (HashMap<String, String>) intent.getSerializableExtra("user");

		// 이벤트 처리 부분

		home.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v){
				Toast.makeText(MenuActivity.this, "홈 메뉴입니다.", Toast.LENGTH_SHORT).show();
			}
		});

		// 오늘 배차 버튼 이벤트
		todayCourseBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				commons comm = new commons();
				String url = distr.common.commons.ServerUrl + "/elms/baecha/ajaxList2.do";

				ArrayList<HashMap<String, String>> elList = (ArrayList<HashMap<String, String>>) comm.SendByHttp3(url, "gs_cd", user.get("gs_cd"));

				if (elList == null) {
					Toast.makeText(MenuActivity.this, "금일 배차정보가 없습니다.", Toast.LENGTH_SHORT).show();
				} else {
					Intent newIntent = new Intent(MenuActivity.this, CourseList.class);
					newIntent.putExtra("user", user);
					newIntent.putExtra("methodType", "REGIST");
					newIntent.putExtra("elList", elList);
					startActivityForResult(newIntent, 0);
				}
			}
		});

		// 로그아웃 버튼 이벤트
		logOutBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent outIntent = new Intent(MenuActivity.this, UserLogin.class);
				Toast.makeText(MenuActivity.this, "로그아웃 되었습니다.", Toast.LENGTH_SHORT).show();
				/*outIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
				outIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(outIntent);
				finish();
			}
		});

		// 배차알람 버튼 이벤트
		courseAlarmBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				commons comm = new commons();

				String url = distr.common.commons.ServerUrl + "/elms/sms/ajaxList2.do";
				ArrayList<HashMap<String, String>> alarmList = (ArrayList<HashMap<String, String>>) comm.SendByHttp2(url, "dest_name", user.get("gs_nm").replaceAll("-", ""), "phone_number", user.get("gs_tel").replaceAll("-", ""));

				if (alarmList == null) {
					Toast.makeText(MenuActivity.this, "배차알림 정보가 없습니다.", Toast.LENGTH_SHORT).show();
				} else {
					Intent newIntent = new Intent(MenuActivity.this, CourseAlarm.class);
					newIntent.putExtra("user", user);
					newIntent.putExtra("alarmList", alarmList);
					startActivityForResult(newIntent, 0);
				}
			}
		});

		// 비밀번호변경 버튼 이벤트
		passChangeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent newIntent = new Intent(MenuActivity.this, UserPassword.class);
				newIntent.putExtra("user", user);
				newIntent.putExtra("methodType", "REGIST");
				startActivityForResult(newIntent, 0);
			}
		});
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		backPress.onBackPressed();
	}
}
