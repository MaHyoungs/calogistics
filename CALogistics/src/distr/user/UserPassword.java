package distr.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import ca.apps.logistics.R;
import distr.common.commons;
import distr.main.MenuActivity;

import java.util.HashMap;

public class UserPassword extends Activity{

	final Context context = this;

	Button home; // title

	EditText orignPass;
	EditText newPass;
	EditText passCheck;
	Button changePass;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_password);

		home = (Button) findViewById(R.id.home);

		orignPass = (EditText)findViewById(R.id.orignPass);
		newPass = (EditText)findViewById(R.id.newPass);
		passCheck = (EditText)findViewById(R.id.passCheck);
		changePass = (Button)findViewById(R.id.changePass );
/*		newPass.setFilters(new InputFilter[] {filterPass});
		passCheck.setFilters(new InputFilter[] {filterPass});*/

		Intent intent = getIntent();
		final HashMap<String, String> user = (HashMap<String, String>) intent.getSerializableExtra("user");

		changePass.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){

				final String orPass = orignPass.getText().toString();
				final String nwPass = newPass.getText().toString();
				final String chckPass = passCheck.getText().toString();

				if(TextUtils.isEmpty(orPass)) {
					Toast.makeText(UserPassword.this, "현재 비밀번호를 입력하세요.", Toast.LENGTH_SHORT).show();
					orignPass.requestFocus();
				} else if(TextUtils.isEmpty(nwPass) || TextUtils.isEmpty(chckPass)) {
					Toast.makeText(UserPassword.this, "변경하실 비밀번호를 입력하세요.", Toast.LENGTH_SHORT).show();
					newPass.requestFocus();
				} else if(!nwPass.trim().matches(chckPass.trim())) {
					Toast.makeText(UserPassword.this, "새로운 비밀번호와 비밀번호 확인이 일치하지 않습니다.", Toast.LENGTH_SHORT).show();
					newPass.requestFocus();
					newPass.setText(null);
					passCheck.setText(null);
				} else {

					if(nwPass.length() < 4) {
						Toast.makeText(UserPassword.this, "비밀번호는 4자리 이상 설정해주세요.", Toast.LENGTH_SHORT).show();
					} else {

						final commons comm = new commons();
						// 현재 비밀번호 기사 체크
						final String chckUrl = distr.common.commons.ServerUrl + "/elms/gisa/ajaxForId.do";

						final String url = distr.common.commons.ServerUrl + "/elms/gisa/ajaxProc.do";

						final String newUrl = distr.common.commons.ServerUrl + "/elms/gisa/ajaxForId.do";

						HashMap<String, String> map = comm.SendByHttpPass(chckUrl, "gs_pw", orPass);

						if(map.get("cl_no") == null || map.get("gs_pw") ==  null) {
							Toast.makeText(UserPassword.this, "비밀번호가 잘못 입력되었습니다.", Toast.LENGTH_SHORT).show();
							orignPass.requestFocus();
							orignPass.setText(null);
						} else {

							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
							alertDialogBuilder
									.setMessage(" 비밀번호를 변경하시겠습니까?")
									.setCancelable(false)
									.setNegativeButton("예",new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id){
											String sojNo = String.valueOf(user.get("soj_no"));
											String gjNo = String.valueOf(user.get("gj_no"));

											HashMap<String, String> map2 = comm.SendByHttpPassInfo(url, "JOB_TYPE", "UPDATE", "cl_cd", user.get("cl_cd"), "gs_nm", user.get("gs_nm"), "gs_tel", user.get("gs_tel")
													, "gs_pw", nwPass, "soj_no", sojNo, "bank_nm", user.get("bank_nm"), "gj_no", gjNo
													, "gj_nm", user.get("gj_nm"), "credit_no", user.get("credit_no"), "check_no", user.get("check_no"), "gs_cd", user.get("gs_cd"), "use_tf", "Y");

											HashMap<String, String> map3 = comm.SendByHttp(newUrl, "cl_no", user.get("cl_no"), "gs_pw", nwPass);

											Toast.makeText(UserPassword.this, "비밀번호가 변경되었습니다. \n       다시 로그인 해주세요.", Toast.LENGTH_SHORT).show();

											SharedPreferences preferences = getSharedPreferences("pref", 0);
											preferences.edit().remove("gisaEditText").commit();

											Intent outIntent = new Intent(UserPassword.this, UserLogin.class);
											outIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
											startActivity(outIntent);
											finish();
										}
									})
									.setPositiveButton("아니요", new DialogInterface.OnClickListener(){
										public void onClick(DialogInterface dialog, int id){

												dialog.cancel();
										}
									});
									// create alert dialog
									AlertDialog alertDialog = alertDialogBuilder.create();
									// show it
									alertDialog.show();

						}
					}
				}

			}
		});

		home.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){
				Intent homeIntent = new Intent(UserPassword.this, MenuActivity.class);
				homeIntent.putExtra("user", user);
				startActivityForResult(homeIntent, 0);
			}
		});
	}

/*	protected InputFilter filterPass = new InputFilter(){

		@Override
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend){

			Pattern ps = Pattern.compile("^[a-zA-Z0-9]+$");

			if(!ps.matcher(source).matches()) {
				return "";
			}

			return null;
		}
	};*/
}
