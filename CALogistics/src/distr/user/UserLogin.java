package distr.user;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import ca.apps.logistics.R;
import distr.common.backPressCloseHandler;
import distr.common.commons;
import distr.intro.IntroActivity;
import distr.main.MenuActivity;

import java.util.HashMap;

public class UserLogin extends Activity{

	private backPressCloseHandler backPress;

	Button loginBtn; // 로그인 버튼
	CheckBox saveChkBox; // 저장 체크박스
	EditText caridEdit; // 차량번호 입력 에디트박스
	EditText gisaEdit; // 비밀번호

	@Override
	protected void onCreate(Bundle savedInstanceState){
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_login);

		startActivity(new Intent(this, IntroActivity.class)); // 인트로 액티비티의 스플래시 뷰 효과

		caridEdit = (EditText)findViewById(R.id.caridEditText);
		gisaEdit = (EditText)findViewById(R.id.gisaEditText);
		loginBtn = (Button)findViewById(R.id.loginButton);
		saveChkBox = (CheckBox)findViewById(R.id.saveCheckBox);

		backPress = new backPressCloseHandler(this);

		// Shared Preference를 불러옴
		SharedPreferences pref = getSharedPreferences("pref", Activity.MODE_PRIVATE);
		// 저장된 값을 꺼내옴
		String caridTxtValue = pref.getString("caridEditText", "");
		String gisaPwTxtValue = pref.getString("gisaEditText", "");
		// 저장된 값을 지정된 위젯에 출력
		caridEdit.setText(caridTxtValue);
		gisaEdit.setText(gisaPwTxtValue);

		saveChkBox.setChecked(pref.getBoolean("saveCheckBox", false));

		// 이벤트 처리내용 기술
		loginBtn.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){
				// TODO Auto-generated method stub

				UserVo user = new UserVo();

				String carid = caridEdit.getText().toString();

				// 차량번호 입력값의 유효성 검사
				if(carid == null || carid.trim().length() < 1)
					return;

				carid = carid.replace("\r", "").replace("\n", "").replace(" ", "").trim();

				boolean bStep = false;

				// 차량번호는 서울33머6666 or 66무3333 의 패턴을 가짐(http://blog.bullsone.com/735 참고)
				if(carid.length() == 9 && carid.matches("[가-힣][가-힣][0-9][0-9][가-힣][0-9][0-9][0-9][0-9]")){
					//Toast.makeText(LoginActivity.this, "지역번호판 차량번호 입니다.", Toast.LENGTH_SHORT).show();
					bStep = true;
				}else if(carid.length() == 7 && carid.matches("[0-9][0-9][가-힣][0-9][0-9][0-9][0-9]")){
					//Toast.makeText(LoginActivity.this, "전국번호판 차량번호 입니다.", Toast.LENGTH_SHORT).show();
					bStep = true;
				}else{
					Toast.makeText(UserLogin.this, "잘못된 차량번호 입니다.", Toast.LENGTH_SHORT).show();
					bStep = false;
				}

				if(bStep){

					user.setCl_no(carid);
					user.setGs_pw(gisaEdit.getText().toString());

					commons comm = new commons();

					//String url = "http://192.168.10.22:8080/elms/gisa/ajaxForId.do";
					String url = distr.common.commons.ServerUrl + "/elms/gisa/ajaxForId.do";

					HashMap<String, String> map = comm.SendByHttp(url, "cl_no", user.getCl_no(), "gs_pw",user.getGs_pw());

					if(map.get("cl_no") == null || map.get("gs_pw") ==  null) {
						Toast.makeText(UserLogin.this, "사용자 정보가 잘못되었습니다.", Toast.LENGTH_SHORT).show();
					} else {

						Intent intent = new Intent(UserLogin.this, MenuActivity.class);

						intent.putExtra("user", map);
						startActivityForResult(intent, 0);
						finish();
					}
				}
			}
		});

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop(){ // 액티비티가 화면에서 사라질 때
		// TODO Auto-generated method stub
		super.onStop();

		// UI 상태를 저장
		SharedPreferences pref = getSharedPreferences("pref", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();

		// 위젯을 불러옴
		EditText edit1 = (EditText)findViewById(R.id.caridEditText);
		EditText edit2 = (EditText)findViewById(R.id.gisaEditText);
		CheckBox chkBox1 = (CheckBox)findViewById(R.id.saveCheckBox);

		boolean bCaridSave = chkBox1.isChecked(); // 차량번호의 저장 체크여부

		// 에디트텍스트에 입력된 문자열을 저장
		editor.putString("caridEditText", (bCaridSave ? edit1.getText().toString() : "")); // 체크되어 있지 않다면 공백문자를 저장
		editor.putString("gisaEditText", (bCaridSave ? edit2.getText().toString() : ""));
		// 체크박스의 상태를 저장
		editor.putBoolean("saveCheckBox", bCaridSave);

		// 저장처리
		editor.commit();
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		backPress.onBackPressed();
	}
}

