/**
 * UserVO
 * @author user
 *
 */

package distr.user;

import java.io.Serializable;

public class UserVo implements Serializable {

	private static final long serialVersionUID = 1L;

	/* 기사고유번호 */
	private String gs_cd;

	/* 아이디 */
	private String gs_id;

	/* 이름 */
	private String gs_nm;

	/* 비밀번호 */
	private String gs_pw;

	/* 전화번호 */
	private String gs_tel;

	/* 차량고유번호 */
	private String cl_cd;

	/* 차량번호 */
	private String cl_no;

	/* 차량분류 */
	private String cl_gb;

	/* 차량분류이름 */
	private String cl_gb_nm;

	/* 차량중량 */
	private String cl_siz;

	/* 차량중량이름 */
	private String cl_siz_nm;

	/* 사업자번호 */
	private String soj_no;

	/* 은행명 */
	private String bank_nm;

	/* 계좌번호 */
	private String gj_no;

	/* 예금주 */
	private String gj_nm;

	/* 신용카드번호 */
	private String credit_no;

	/* 체크카드번호 */
	private String check_no;

	/* 등록일자 */
	private String regist_date;

	/* 수정일자 */
	private String update_date;

	/* 사용여부 */
	private String use_yn;

	public String getGs_cd(){
		return gs_cd;
	}

	public void setGs_cd(String gs_cd){
		this.gs_cd = gs_cd;
	}

	public String getGs_id(){
		return gs_id;
	}

	public void setGs_id(String gs_id){
		this.gs_id = gs_id;
	}

	public String getGs_nm(){
		return gs_nm;
	}

	public void setGs_nm(String gs_nm){
		this.gs_nm = gs_nm;
	}

	public String getGs_pw(){
		return gs_pw;
	}

	public void setGs_pw(String gs_pw){
		this.gs_pw = gs_pw;
	}

	public String getGs_tel(){
		return gs_tel;
	}

	public void setGs_tel(String gs_tel){
		this.gs_tel = gs_tel;
	}

	public String getCl_cd(){
		return cl_cd;
	}

	public void setCl_cd(String cl_cd){
		this.cl_cd = cl_cd;
	}

	public String getCl_no(){
		return cl_no;
	}

	public void setCl_no(String cl_no){
		this.cl_no = cl_no;
	}

	public String getCl_gb(){
		return cl_gb;
	}

	public void setCl_gb(String cl_gb){
		this.cl_gb = cl_gb;
	}

	public String getCl_siz(){
		return cl_siz;
	}

	public void setCl_siz(String cl_siz){
		this.cl_siz = cl_siz;
	}

	public String getSoj_no(){
		return soj_no;
	}

	public void setSoj_no(String soj_no){
		this.soj_no = soj_no;
	}

	public String getBank_nm(){
		return bank_nm;
	}

	public void setBank_nm(String bank_nm){
		this.bank_nm = bank_nm;
	}

	public String getGj_no(){
		return gj_no;
	}

	public void setGj_no(String gj_no){
		this.gj_no = gj_no;
	}

	public String getGj_nm(){
		return gj_nm;
	}

	public void setGj_nm(String gj_nm){
		this.gj_nm = gj_nm;
	}

	public String getCredit_no(){
		return credit_no;
	}

	public void setCredit_no(String credit_no){
		this.credit_no = credit_no;
	}

	public String getCheck_no(){
		return check_no;
	}

	public void setCheck_no(String check_no){
		this.check_no = check_no;
	}

	public String getRegist_date(){
		return regist_date;
	}

	public void setRegist_date(String regist_date){
		this.regist_date = regist_date;
	}

	public String getUpdate_date(){
		return update_date;
	}

	public void setUpdate_date(String update_date){
		this.update_date = update_date;
	}

	public String getUse_yn(){
		return use_yn;
	}

	public void setUse_yn(String use_yn){
		this.use_yn = use_yn;
	}

	public String getCl_gb_nm(){
		return cl_gb_nm;
	}

	public void setCl_gb_nm(String cl_gb_nm){
		this.cl_gb_nm = cl_gb_nm;
	}

	public String getCl_siz_nm(){
		return cl_siz_nm;
	}

	public void setCl_siz_nm(String cl_siz_nm){
		this.cl_siz_nm = cl_siz_nm;
	}
}
