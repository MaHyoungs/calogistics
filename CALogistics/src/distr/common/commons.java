package distr.common;

import android.os.StrictMode;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class commons{

	public static final String ServerUrl = "http://elms.cheongahm.co.kr";
	/*public static final String ServerUrl = "http://192.168.10.22:8080";*/

	// 응답결과가 JSON 객체 단일 하나
	public HashMap<String, String> SendByHttp(String url, String sendName, String sendValue, String sendName2, String sendValue2){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null || sendValue2 == null){
			sendValue = "";
			sendValue2 = "";
		}

		/*String URL = "http://www.naver.com";*/
		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue + "&" + sendName2 + "=" + sendValue2);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));
			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;
			JSONObject result = (JSONObject)res.get("rs");

			HashMap<String, String> map = new HashMap<String, String>();

			if(result == null){
				return map;
			}

			map.putAll(result);
			return map;

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}

	// 응답결과가 JSON 객체 단일 하나 ( 값 하나)
	public HashMap<String, String> SendByHttpPass(String url, String sendName, String sendValue){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null){
			sendValue = "";
		}

		/*String URL = "http://www.naver.com";*/
		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));
			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;
			JSONObject result = (JSONObject)res.get("rs");

			HashMap<String, String> map = new HashMap<String, String>();

			if(result == null){
				return map;
			}

			map.putAll(result);
			return map;

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}

	// 응답결과가 JSON 객체리스트
	public List SendByHttp2(String url, String sendName, String sendValue, String sendName2, String sendValue2){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null || sendValue2 == null){
			sendValue = "";
			sendValue2 = "";
		}

		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue + "&" + sendName2 + "=" + sendValue2);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;

			//List elList = new ArrayList();
			ArrayList<HashMap<String, String>> elList = new ArrayList<HashMap<String, String>>();
			JSONArray array = (JSONArray)res.get("rs");
			for (int i = 0; i < array.size(); i++) {
				JSONObject elItem = (JSONObject) array.get(i);
				HashMap<String, String> map = new HashMap<String, String>();
				map.putAll(elItem);
				elList.add(map);
			}

			//			for(int i = 0; i < elList.size(); i++){
			//				Log.i("elList.get(" + i + ")", elList.get(i).toString());
			//			}

			if (elList.size() < 1)
				return null;
			else
				return elList;

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}

	// 응답결과가 JSON 객체리스트
	public List SendByHttp3(String url, String sendName, String sendValue){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null){
			sendValue = "";
		}

		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;

			//List elList = new ArrayList();
			ArrayList<HashMap<String, String>> elList = new ArrayList<HashMap<String, String>>();
			JSONArray array = (JSONArray)res.get("rs");
			for (int i = 0; i < array.size(); i++) {
				JSONObject elItem = (JSONObject) array.get(i);
				HashMap<String, String> map = new HashMap<String, String>();
				map.putAll(elItem);
				elList.add(map);
			}

			//			for(int i = 0; i < elList.size(); i++){
			//				Log.i("elList.get(" + i + ")", elList.get(i).toString());
			//			}

			if (elList.size() < 1)
				return null;
			else
				return elList;

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}

	// update Busokpumsu
	public String SendByHttp4(String url, String sendName, String sendValue, String sendName1, String sendValue1,
	                          String sendName2, String sendValue2, String sendName3, String sendValue3, String sendName4, String sendValue4, String sendName5, String sendValue5){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null){
			sendValue = "";
		}

		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue + "&" + sendName1 + "=" + sendValue1 + "&" + sendName2 + "=" + sendValue2
					+ "&" + sendName3 + "=" + sendValue3 + "&" + sendName4 + "=" + sendValue4 + "&" + sendName5 + "=" + sendValue5);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));
			/*R_ERR_MESSAGE*/

			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;
			JSONObject result = (JSONObject)res.get("rs");

			HashMap<String, String> map = new HashMap<String, String>();

			if(result == null){
				return "fail";
			}

			map.putAll(result);

			return "success";

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}

	// delete Rac
	public String SendByHttp5(String url, String sendName, String sendValue, String sendName1, String sendValue1){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null){
			sendValue = "";
		}

		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue + "&" + sendName1 + "=" + sendValue1);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));
			/*R_ERR_MESSAGE*/

			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;
			JSONObject result = (JSONObject)res.get("rs");

			HashMap<String, String> map = new HashMap<String, String>();

			if(result == null){
				return "fail";
			}

			map.putAll(result);

			return "success";

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}

	// insert Rac
	public HashMap<String, String> SendByHttp6(String url, String sendName, String sendValue, String sendName1, String sendValue1,
	                                           String sendName2, String sendValue2, String sendName3, String sendValue3){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null){
			sendValue = "";
		}

		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue + "&" + sendName1 + "=" + sendValue1 + "&" + sendName2 + "=" + sendValue2
					+ "&" + sendName3 + "=" + sendValue3);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));
			/*R_ERR_MESSAGE*/

			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;
			JSONObject result = (JSONObject)res.get("rs");

			HashMap<String, String> map = new HashMap<String, String>();

			if(result == null){
				return map;
			}

			map.putAll(result);

			return map;

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}

	// update Password
	public HashMap<String, String> SendByHttpPassInfo(String url, String sendName, String sendValue, String sendName2, String sendValue2, String sendName3, String sendValue3
			, String sendName4, String sendValue4, String sendName5, String sendValue5, String sendName6, String sendValue6, String sendName7, String sendValue7
			, String sendName8, String sendValue8, String sendName9, String sendValue9, String sendName10, String sendValue10, String sendName11, String sendValue11
			, String sendName12, String sendValue12, String sendName13, String sendValue13){

		if(android.os.Build.VERSION.SDK_INT > 9){
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);

		}

		if(sendValue == null){
			sendValue = "";
		}

		/*String URL = "http://www.naver.com";*/
		DefaultHttpClient client = new DefaultHttpClient();
		try{
			/* 체크할 id와 pwd값 서버로 전송 */
			HttpPost post = new HttpPost(url + "?" + sendName + "=" + sendValue + "&" + sendName2 + "=" + sendValue2 + "&" + sendName3 + "=" + sendValue3 + "&" + sendName4 + "=" + sendValue4
					+ "&" + sendName5 + "=" + sendValue5 + "&" + sendName6 + "=" + sendValue6 + "&" + sendName7 + "=" + sendValue7 + "&" + sendName8 + "=" + sendValue8
					+ "&" + sendName9 + "=" + sendValue9 + "&" + sendName10 + "=" + sendValue10 + "&" + sendName11 + "=" + sendValue11 + "&" + sendName12 + "=" + sendValue12
					+ "&" + sendName13 + "=" + sendValue13);
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);

			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));
			Object obj = JSONValue.parse(in);
			JSONObject res = (JSONObject)obj;
			JSONObject result = (JSONObject)res.get("rs");

			HashMap<String, String> map = new HashMap<String, String>();

			if(result == null){
				return map;
			}

			map.putAll(result);
			return map;

		}catch(Exception e){
			e.printStackTrace();
			client.getConnectionManager().shutdown();    // 연결 지연 종료
			return null;
		}
	}
}
