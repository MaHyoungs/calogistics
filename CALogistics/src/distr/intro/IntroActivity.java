package distr.intro;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import ca.apps.logistics.R;

public class IntroActivity extends Activity {

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.intro_activity);

		ImageView image = (ImageView) findViewById(R.id.intro);
		Animation alpahAnim = AnimationUtils.loadAnimation(this, R.anim.alpha);
		image.startAnimation(alpahAnim);

		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				finish();
			};
		};

		handler.sendEmptyMessageDelayed(0, 2000); // 두번째 인자로 시간 조정가능
	}

}
