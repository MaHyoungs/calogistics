package distr.course.bacha;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import ca.apps.logistics.R;
import distr.common.commons;
import distr.course.CourseList;
import distr.main.MenuActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class BachaInfo extends Activity {

	final Context context = this;

	// title bar
	Button home;

	// Gisa Info
	private EditText day_edit;
	private EditText name_edit;
	private EditText carNum_edit;
	private EditText start_edit;
	private EditText desti_edit;

	// Rac Info
	private LinearLayout racArea;
	private Button racAdd;
	final ArrayList<HashMap<String, String>> resultRacs = new ArrayList<HashMap<String, String>>();
	final HashMap<String, String> racCount = new HashMap<String, String>();
	final HashMap<String, String> editIdCnt = new HashMap<String, String>();

	// Etc Info
	EditText etc_edit1;
	EditText etc_edit2;
	EditText etc_edit3;
	EditText etc_edit4;
	EditText etc_edit5;
	String edit1 = "";
	String edit2 = "";
	String edit3 = "";
	String edit4 = "";

	// submit & cancel button
	private Button submit;
	private Button cancel;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_bachainfo);

		// 공통 dp ( 사용할 DIP )
		final float scale = getResources().getDisplayMetrics().density;
		int dp_10 = (int) (10 * scale);
		int dp_20 = (int) (20 * scale);
		int dp_30 = (int) (30 * scale);
		final int dp_33 = (int) (33 * scale);
		int dp_35 = (int) (35 * scale);
		final int dp_45 = (int) (43 * scale);
		final int dp_85 = (int) (83 * scale);
		final int dp_125 = (int) (123 * scale);
		final int dp_165 = (int) (163 * scale);
		final int dp_205 = (int) (203 * scale);
		final int dp_245 = (int) (243 * scale);
		final int dp_285 = (int) (283 * scale);

		// 필요한 값 가져오기 gisa_info, rac_info, etc_info, login_info
		Intent intent = getIntent();
		final HashMap<String, String> gisaInfo = (HashMap<String, String>) intent.getSerializableExtra("gisaInfo");
		final ArrayList<HashMap<String, String>> racInfo = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("racInfo");
		final ArrayList<HashMap<String, String>> etcInfo = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("etcInfo");
		final HashMap<String, String> user = (HashMap<String, String>) intent.getSerializableExtra("user");

		// title bar start
		home = (Button) findViewById(R.id.home);

		home.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){
				Intent homeIntent = new Intent(BachaInfo.this, MenuActivity.class);
				homeIntent.putExtra("user", user);
				startActivityForResult(homeIntent, 0);
			}
		});
		// title bar end

		// gisa info end
		day_edit = (EditText) findViewById(R.id.dayEdit);
		name_edit = (EditText) findViewById(R.id.nameEdit);
		carNum_edit = (EditText) findViewById(R.id.carNumEdit);
		start_edit = (EditText)findViewById(R.id.startEdit);
		desti_edit = (EditText)findViewById(R.id.destiEdit);

		day_edit.setText(gisaInfo.get("bc_dt"));
		name_edit.setText(gisaInfo.get("gs_nm"));
		carNum_edit.setText(gisaInfo.get("cl_no"));
		start_edit.setText(gisaInfo.get("plc_st_nm"));
		desti_edit.setText(gisaInfo.get("plc_en_nm"));
		// gisa info end

		// rac Info 초기 설정 start
		racArea = (LinearLayout) findViewById(R.id.racArea);
		racAdd = (Button) findViewById(R.id.racAdd);
		final RelativeLayout.LayoutParams racParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams3 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams4 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams5 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams6 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams7 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams8 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams9 = new RelativeLayout.LayoutParams(dp_35, dp_35);
		final LinearLayout.LayoutParams marginTparam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		if ( racInfo != null) {

			final int racSize = racInfo.size();
			int idValue=200;

			for(int idxx=0; idxx<racSize; idxx++) {

				// rac textView, editView 일련 번호 처리
				HashMap<String, String> resuRac = new HashMap<String, String>();
				HashMap<String, String> racs = racInfo.get(idxx);
				resuRac.put("txtId", String.valueOf((idxx + 1) + 100));
				resuRac.put("editId", String.valueOf(idValue));
				racCount.put("cnt", String.valueOf(idxx + 1));
				resultRacs.add(resuRac);
				resuRac = new HashMap<String, String>();

				// rac layout 생성
				final RelativeLayout upper = new RelativeLayout(BachaInfo.this);
				upper.setLayoutParams(racParams);

				// gap layout 생성
				marginTparam.topMargin = dp_10;
				final LinearLayout marginT = new LinearLayout(BachaInfo.this);
				marginT.setLayoutParams(marginTparam);

				// rac Text 생성
				final TextView txt = new TextView(this);
				txt.setId((idxx + 1) + 100);
				txt.setText("  R" + (idxx + 1));
				txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_bullet, 0, 0, 0);
				txt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
				racParams2.addRule(RelativeLayout.CENTER_VERTICAL);
				txt.setLayoutParams(racParams2);

				upper.addView(txt);

				// rac edit 생성
				for(int dx=0; dx<6;dx++) {

					final EditText edit = new EditText(this);
					edit.setId((idxx+1) + (dx+1) + idValue);
					if(dx < 4) {
						edit.setRawInputType(EditorInfo.TYPE_CLASS_NUMBER);
					} else {
						edit.setRawInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_VARIATION_URI);
					}
					edit.setImeOptions(EditorInfo.IME_ACTION_NEXT);
					edit.setText(racs.get("rc_cd").substring(dx, dx + 1));
					edit.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
					edit.setEms(1);
					edit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
					edit.setBackgroundDrawable(getResources().getDrawable(R.drawable.border3));
					edit.setSelectAllOnFocus(true);
					edit.setGravity(Gravity.CENTER);

					if( dx==0) {
						racParams3.leftMargin = dp_45;
						edit.setLayoutParams(racParams3);
					} else if( dx==1) {
						racParams4.leftMargin = dp_85;
						edit.setLayoutParams(racParams4);
					} else if( dx==2) {
						racParams5.leftMargin = dp_125;
						edit.setLayoutParams(racParams5);
					} else if( dx==3) {
						racParams6.leftMargin = dp_165;
						edit.setLayoutParams(racParams6);
					} else if( dx==4) {
						racParams7.leftMargin = dp_205;
						edit.setLayoutParams(racParams7);
					} else {
						racParams8.leftMargin = dp_245;
						edit.setLayoutParams(racParams8);
					}
					final String num = String.valueOf((idxx + 1) + (dx + 2) + idValue);
					final String nexNum = String.valueOf((idxx + 2) + (1) + idValue + 100);
					final int cnts = dx;
					if(dx < 4) {
						edit.addTextChangedListener(new TextWatcher(){
                            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
                            @Override public void afterTextChanged(Editable s){}
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count){
	                            if(s.toString().length() > 0){
		                            EditText t1 = (EditText)findViewById(getResources().getIdentifier(num, "id", getPackageName()));
		                            t1.requestFocus();
	                            }
                            }
						});
					} else {
						edit.addTextChangedListener(new TextWatcher(){
							@Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
							@Override public void afterTextChanged(Editable s){}
							@Override
							public void onTextChanged(CharSequence s, int start, int before, int count){
								if(s.toString().length() > 0){
									if(!s.toString().matches("[A-Za-z]")){
										Toast.makeText(BachaInfo.this, "영문만 입력하실 수 있습니다.", Toast.LENGTH_SHORT).show();
										edit.setText(null);
									}else if(s.toString().matches("[a-z]")){
										edit.setText(s.toString().toUpperCase());
										edit.setSelection(edit.length());

										if(cnts < 5){
											EditText t1 = (EditText)findViewById(getResources().getIdentifier(num, "id", getPackageName()));
											t1.requestFocus();
										} else {
												EditText t1 = (EditText)findViewById(getResources().getIdentifier(nexNum, "id", getPackageName()));
											if(t1 != null){
												t1.requestFocus();
											}
										}
									}else{
										if(cnts < 5){
											EditText t1 = (EditText)findViewById(getResources().getIdentifier(num, "id", getPackageName()));
											t1.requestFocus();
										} else {
											EditText t1 = (EditText)findViewById(getResources().getIdentifier(nexNum, "id", getPackageName()));
											if(t1 != null){
												t1.requestFocus();
											}
										}
									}
								}
							}
						});
					}
					upper.addView(edit);

				}

				editIdCnt.put("editId", String.valueOf(idValue));
				idValue += 100;

				racParams9.leftMargin = dp_285;

				final ImageButton delBtn = new ImageButton(this);
				delBtn.setId((idxx+1) + 100);
				delBtn.setImageDrawable(getResources().getDrawable(R.drawable.icon_del));
				delBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.border2));
				delBtn.setOnClickListener(new View.OnClickListener(){

					@Override
					public void onClick(View v){

						racArea.removeView(upper);
						racArea.removeView(marginT);

						for(int z=0; z<resultRacs.size(); z++) {
							HashMap<String, String> delinfo = resultRacs.get(z);
							int delTxtId = Integer.parseInt(delinfo.get("txtId"));
							if(delBtn.getId() == delTxtId) {
								resultRacs.remove(z);
								break;
							}
						}

						for(int y=0; y<resultRacs.size(); y++) {
							HashMap<String, String> upTxt = resultRacs.get(y);
							TextView newTxt = (TextView)findViewById(getResources().getIdentifier(String.valueOf(Integer.parseInt(upTxt.get("txtId"))), "id", getPackageName()));
							newTxt.setText("  R" + (y + 1));
						}

					}
				});

				racParams9.addRule(RelativeLayout.CENTER_VERTICAL);
				delBtn.setLayoutParams(racParams9);

				upper.addView(delBtn);
				upper.setId(idxx+77);

				racArea.addView(upper);
				racArea.addView(marginT);
			}

		}
		// rac Info 초기 설정 end

		// rac Info 추가 start
		racAdd.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){

				final RelativeLayout newUpper = new RelativeLayout(BachaInfo.this);
				newUpper.setLayoutParams(racParams);

				// gap
				final LinearLayout marginT = new LinearLayout(BachaInfo.this);
				marginT.setLayoutParams(marginTparam);

				racArea = (LinearLayout) findViewById(R.id.racArea);

				newUpper.addView(createNewTextView(racParams2));

				for(int dxxx=0; dxxx<6;dxxx++){

					if( dxxx==0) {
						racParams3.leftMargin = dp_45;
						newUpper.addView(createNewEditView(dxxx, racParams3));
					} else if( dxxx==1) {
						racParams4.leftMargin = dp_85;
						newUpper.addView(createNewEditView(dxxx, racParams4));
					} else if( dxxx==2) {
						racParams5.leftMargin = dp_125;
						newUpper.addView(createNewEditView(dxxx, racParams5));
					} else if( dxxx==3) {
						racParams6.leftMargin = dp_165;
						newUpper.addView(createNewEditView(dxxx, racParams6));
					} else if( dxxx==4) {
						racParams7.leftMargin = dp_205;
						newUpper.addView(createNewEditView(dxxx, racParams7));
					} else {
						racParams8.leftMargin = dp_245;
						newUpper.addView(createNewEditView(dxxx, racParams8));
					}
				}

				racParams9.leftMargin = dp_285;
				racParams9.addRule(RelativeLayout.CENTER_VERTICAL);
				newUpper.addView(createDeleteButton(newUpper, marginT, racParams9));
				newUpper.setId(resultRacs.size()+77);

				racCount.put("cnt", String.valueOf((Integer.parseInt(racCount.get("cnt")) + 1)));
				editIdCnt.put("editId", String.valueOf((Integer.parseInt(editIdCnt.get("editId")) + 100)));
				racArea.addView(newUpper);
				racArea.addView(marginT);
			}

		});
		// rac Info 추가 end

		// etc info start
		etc_edit1 = (EditText) findViewById(R.id.etcEdit1);
		etc_edit2 = (EditText) findViewById(R.id.etcEdit2);
		etc_edit3 = (EditText) findViewById(R.id.etcEdit3);
		etc_edit4 = (EditText) findViewById(R.id.etcEdit4);
		etc_edit5 = (EditText) findViewById(R.id.etcEdit5);
		etc_edit1.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
		etc_edit2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
		etc_edit3.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
		etc_edit4.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});

		// comma process
		final DecimalFormat df = new DecimalFormat("###,###,###,###");

		if (etcInfo != null){

			int listMax = etcInfo.size();
			for (int idx=0; idx<listMax; idx++) {
				HashMap<String, String> map = etcInfo.get(idx);
				String bsp = map.get("bsp_ccd");
				String cnt = df.format(map.get("bsp_cnt"));

				if(bsp.equals("BSPCCD_01")) {
					etc_edit1.setText(cnt);

				} else if(bsp.equals("BSPCCD_02")) {
					etc_edit2.setText(cnt);

				} else if(bsp.equals("BSPCCD_03")){
					etc_edit3.setText(cnt);

				} else if(bsp.equals("BSPCCD_04")){
					etc_edit4.setText(cnt);
				}
			}
		}
		etc_edit5.setText(gisaInfo.get("gj_dc"));

		// T바
		etc_edit1.addTextChangedListener(new TextWatcher(){
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			@Override public void afterTextChanged(Editable s){}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){
				if(!s.toString().equals(edit1)) {
					if(s.toString().length() > 1){
						if(!s.toString().contains(".")){
							edit1 = df.format(Integer.parseInt(s.toString().replaceAll(",", "")));
							etc_edit1.setText(edit1);
							etc_edit1.setSelection(edit1.length());
						}
					}
				}
			}
		});

		// 꽂이대
		etc_edit2.addTextChangedListener(new TextWatcher(){
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			@Override public void afterTextChanged(Editable s){}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){
				if(!s.toString().equals(edit2)) {
					if(s.toString().length() > 1){
						if(!s.toString().contains(".")){
							edit2 = df.format(Integer.parseInt(s.toString().replaceAll(",", "")));
							etc_edit2.setText(edit2);
							etc_edit2.setSelection(edit2.length());
						}
					}
				}
			}
		});

		// 스크랩(백색)
		etc_edit3.addTextChangedListener(new TextWatcher(){
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			@Override public void afterTextChanged(Editable s){}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){
				if(!s.toString().equals(edit3)) {
					if(s.toString().length() > 1){
						if(!s.toString().contains(".")){
							edit3 = df.format(Integer.parseInt(s.toString().replaceAll(",", "")));
							etc_edit3.setText(edit3);
							etc_edit3.setSelection(edit3.length());
						}
					}
				}
			}
		});

		// 스크랩(유색)
		etc_edit4.addTextChangedListener(new TextWatcher(){
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			@Override public void afterTextChanged(Editable s){}
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){
				if(!s.toString().equals(edit4)) {
					if(s.toString().length() > 1){
						if(!s.toString().contains(".")){
							edit4 = df.format(Integer.parseInt(s.toString().replaceAll(",", "")));
							etc_edit4.setText(edit4);
							etc_edit4.setSelection(edit4.length());
						}
					}
				}
			}
		});

		// etc info end

		// submit & cancel start
		submit = (Button) findViewById(R.id.submit);
		cancel = (Button) findViewById(R.id.cancel);

		if(gisaInfo.get("uh_gb").equals("BCSTGB_03")) {
			submit.setText("재승인요청");
		}

		submit.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){

				if(gisaInfo.get("uh_gb").equals("BCSTGB_03")) {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder
							.setMessage("   재승인요청 하시겠습니까?")
							.setCancelable(false)
							.setNegativeButton("예",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {

									commons comm = new commons();

									//rac Info delete & insert
									String racDelUrls = distr.common.commons.ServerUrl + "/elms/bcrack/ajaxProc.do";
									comm.SendByHttp5(racDelUrls, "job_type", "ALL_DELETE", "bcuh_cd", gisaInfo.get("bcuh_cd"));

									String racInsertUrls = distr.common.commons.ServerUrl + "/elms/bcrack/ajaxProc.do";

									for(int t = 0; t < resultRacs.size(); t++){
										HashMap<String, String> upEdit = resultRacs.get(t);
										String editString = "";
										Log.i("txId", upEdit.get("txtId"));

										for(int c = 0; c < 6; c++){

											EditText newEdit = (EditText)findViewById(getResources().getIdentifier(String.valueOf((Integer.parseInt(upEdit.get("txtId")) - 100 + Integer.parseInt(upEdit.get("editId")) + (c + 1))), "id", getPackageName()));
											editString += newEdit.getText().toString();
										}
										comm.SendByHttp6(racInsertUrls, "job_type", "INSERT", "rc_cd", editString, "bcuh_cd", gisaInfo.get("bcuh_cd"), "bc_cd", gisaInfo.get("bc_cd"));
									}

									//etc Info update
									if(etcInfo != null){

										int listMax = etcInfo.size();
										for(int idx = 0; idx < listMax; idx++){
											HashMap<String, String> map = etcInfo.get(idx);
											String bsp = map.get("bsp_ccd");
											String getCnt;

											if(bsp.equals("BSPCCD_01")){
												getCnt = etc_edit1.getText().toString().replaceAll(",", "");
												map.put("bsp_cnt", getCnt);

											}else if(bsp.equals("BSPCCD_02")){
												getCnt = etc_edit2.getText().toString().replaceAll(",","");
												map.put("bsp_cnt", getCnt);

											}else if(bsp.equals("BSPCCD_03")){
												getCnt = etc_edit3.getText().toString().replaceAll(",","");
												map.put("bsp_cnt", getCnt);

											}else if(bsp.equals("BSPCCD_04")){
												getCnt = etc_edit4.getText().toString().replaceAll(",","");
												map.put("bsp_cnt", getCnt);
											}

											String urls = distr.common.commons.ServerUrl + "/elms/bcbusokpumsu/ajaxProc.do";

											comm.SendByHttp4(urls, "job_type", "UPDATE", "bcbsp_cd", map.get("bcbsp_cd"), "bcuh_cd", map.get("bcuh_cd"), "bc_cd", map.get("bc_cd"), "bsp_ccd", map.get("bsp_ccd"), "bsp_cnt", map.get("bsp_cnt"));
										}
									}

									// 이거 아니면 confirm으로
									Toast.makeText(BachaInfo.this, "재승인요청되었습니다.", Toast.LENGTH_SHORT).show();

									String url = distr.common.commons.ServerUrl + "/elms/baecha/ajaxList2.do";
									ArrayList<HashMap<String, String>> elList = (ArrayList<HashMap<String, String>>) comm.SendByHttp3(url, "gs_cd", user.get("gs_cd"));

									// 승인 후 메인메뉴로
									Intent nextIntent = new Intent(BachaInfo.this, CourseList.class);
									nextIntent.putExtra("user", user);
									nextIntent.putExtra("elList", elList);
									startActivityForResult(nextIntent, 0);
								}
							})
							.setPositiveButton("아니요", new DialogInterface.OnClickListener(){
								public void onClick(DialogInterface dialog, int id){

									dialog.cancel();
								}
							});
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();

				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
					alertDialogBuilder
							.setMessage("   승인요청 하시겠습니까?")
							.setCancelable(false)
							.setNegativeButton("예",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {

									commons comm = new commons();

									//rac Info delete & insert
									String racDelUrls = distr.common.commons.ServerUrl + "/elms/bcrack/ajaxProc.do";
									comm.SendByHttp5(racDelUrls, "job_type", "ALL_DELETE", "bcuh_cd", gisaInfo.get("bcuh_cd"));

									String racInsertUrls = distr.common.commons.ServerUrl + "/elms/bcrack/ajaxProc.do";

									for(int t = 0; t < resultRacs.size(); t++){
										HashMap<String, String> upEdit = resultRacs.get(t);
										String editString = "";
										Log.i("txId", upEdit.get("txtId"));

										for(int c = 0; c < 6; c++){

											EditText newEdit = (EditText)findViewById(getResources().getIdentifier(String.valueOf((Integer.parseInt(upEdit.get("txtId")) - 100 + Integer.parseInt(upEdit.get("editId")) + (c + 1))), "id", getPackageName()));
											editString += newEdit.getText().toString();
										}
										comm.SendByHttp6(racInsertUrls, "job_type", "INSERT", "rc_cd", editString, "bcuh_cd", gisaInfo.get("bcuh_cd"), "bc_cd", gisaInfo.get("bc_cd"));
									}

									//etc Info update
									if(etcInfo != null){

										int listMax = etcInfo.size();
										for(int idx = 0; idx < listMax; idx++){
											HashMap<String, String> map = etcInfo.get(idx);
											String bsp = map.get("bsp_ccd");
											String getCnt;

											if(bsp.equals("BSPCCD_01")){
												getCnt = etc_edit1.getText().toString().replaceAll(",","");
												map.put("bsp_cnt", getCnt);

											}else if(bsp.equals("BSPCCD_02")){
												getCnt = etc_edit2.getText().toString().replaceAll(",", "");
												map.put("bsp_cnt", getCnt);

											}else if(bsp.equals("BSPCCD_03")){
												getCnt = etc_edit3.getText().toString().replaceAll(",", "");
												map.put("bsp_cnt", getCnt);

											}else if(bsp.equals("BSPCCD_04")){
												getCnt = etc_edit4.getText().toString().replaceAll(",","");
												map.put("bsp_cnt", getCnt);
											}

											String urls = distr.common.commons.ServerUrl + "/elms/bcbusokpumsu/ajaxProc.do";

											comm.SendByHttp4(urls, "job_type", "UPDATE", "bcbsp_cd", map.get("bcbsp_cd"), "bcuh_cd", map.get("bcuh_cd"), "bc_cd", map.get("bc_cd"), "bsp_ccd", map.get("bsp_ccd"), "bsp_cnt", map.get("bsp_cnt"));
										}
									}

									// update bc_st
									String upUrls = distr.common.commons.ServerUrl + "/elms/baecha/ajaxStatusUpdate.do";
									comm.SendByHttp2(upUrls, "bc_cd", gisaInfo.get("bc_cd"), "uh_gb", "BCSTGB_03");


									// 이거 아니면 confirm으로
									Toast.makeText(BachaInfo.this, "승인요청되었습니다.", Toast.LENGTH_SHORT).show();

									String url = distr.common.commons.ServerUrl + "/elms/baecha/ajaxList2.do";
									ArrayList<HashMap<String, String>> elList = (ArrayList<HashMap<String, String>>) comm.SendByHttp3(url, "gs_cd", user.get("gs_cd"));

									// 승인 후 메인메뉴로
									Intent nextIntent = new Intent(BachaInfo.this, CourseList.class);
									nextIntent.putExtra("user", user);
									nextIntent.putExtra("elList", elList);
									startActivityForResult(nextIntent, 0);
								}
							})
							.setPositiveButton("아니요", new DialogInterface.OnClickListener(){
								public void onClick(DialogInterface dialog, int id){

									dialog.cancel();
								}
							});
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
					// show it
					alertDialog.show();
				}

			}
		});

		cancel.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){

				commons comm = new commons();
				String url = distr.common.commons.ServerUrl + "/elms/baecha/ajaxList2.do";

				ArrayList<HashMap<String, String>> elList = (ArrayList<HashMap<String, String>>) comm.SendByHttp3(url, "gs_cd", user.get("gs_cd"));

				Intent backIntent = new Intent(BachaInfo.this, CourseList.class);
				backIntent.putExtra("user", user);
				backIntent.putExtra("elList", elList);
				startActivityForResult(backIntent, 0);
			}
		});
	}
	// submit & cancel end

	// rac 추가 버튼
	private TextView createNewTextView(final RelativeLayout.LayoutParams racParams2) {

		// rac textView, editView 일련 번호 처리
		HashMap<String, String> newResuRac = new HashMap<String, String>();
		newResuRac.put("txtId", String.valueOf((Integer.parseInt(racCount.get("cnt")) + 1 + 100)));
		newResuRac.put("editId", String.valueOf((Integer.parseInt(editIdCnt.get("editId")) +100)));

		final TextView textView = new TextView(this);
		textView.setId((Integer.parseInt(racCount.get("cnt")) + 1 + 100));
		textView.setText("  R" + (resultRacs.size() + 1));
		textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_bullet, 0, 0, 0);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
		racParams2.addRule(RelativeLayout.CENTER_VERTICAL);
		textView.setLayoutParams(racParams2);

		resultRacs.add(newResuRac);
		newResuRac = new HashMap<String, String>();

		return textView;
	}

	// rac 추가 버튼
	private EditText createNewEditView(int dxx, final RelativeLayout.LayoutParams racParams) {
		final EditText editText = new EditText(this);
		editText.setId((Integer.parseInt(racCount.get("cnt")) + 1 + (dxx+1) + Integer.parseInt(editIdCnt.get("editId")) + 100));
		if(dxx < 4) {
			editText.setRawInputType(EditorInfo.TYPE_CLASS_NUMBER);
		} else {
			editText.setRawInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_VARIATION_URI);
		}
		editText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		editText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
		editText.setEms(1);
		editText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(1)});
		editText.setSelectAllOnFocus(true);
		editText.setBackgroundDrawable(getResources().getDrawable(R.drawable.border3));
		editText.setGravity(Gravity.CENTER);
		editText.setLayoutParams(racParams);
		final String num2 = String.valueOf((Integer.parseInt(racCount.get("cnt")) + 1 + (dxx+2) + Integer.parseInt(editIdCnt.get("editId")) + 100));
		final String nexNum2 = String.valueOf((Integer.parseInt(racCount.get("cnt")) + 1 + 2 + Integer.parseInt(editIdCnt.get("editId")) + 100) + 100);
		final int cnts2 = dxx;
		if(dxx < 4) {
			editText.addTextChangedListener(new TextWatcher(){
				@Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
				@Override public void afterTextChanged(Editable s){}
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count){
					if(s.toString().length() > 0){
						EditText t2 = (EditText)findViewById(getResources().getIdentifier(num2, "id", getPackageName()));
						t2.requestFocus();
					}
				}
            });
		} else{
			editText.addTextChangedListener(new TextWatcher(){
				@Override public void beforeTextChanged(CharSequence s, int start, int count, int after){}
				@Override public void afterTextChanged(Editable s){}
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count){
					if(s.toString().length() > 0){
						if(!s.toString().matches("[A-Za-z]")){
							Toast.makeText(BachaInfo.this, "영문만 입력하실 수 있습니다.", Toast.LENGTH_SHORT).show();
							editText.setText(null);
						}else if(s.toString().matches("[a-z]")){
							editText.setText(s.toString().toUpperCase());
							editText.setSelection(editText.length());

							if(cnts2 < 5){
								EditText t2 = (EditText)findViewById(getResources().getIdentifier(num2, "id", getPackageName()));
								t2.requestFocus();
							} else {
								EditText t2 = (EditText)findViewById(getResources().getIdentifier(nexNum2, "id", getPackageName()));
								if(t2 != null){
									t2.requestFocus();
								}
							}
						}else{
							if(cnts2 < 5){
								EditText t2 = (EditText)findViewById(getResources().getIdentifier(num2, "id", getPackageName()));
								t2.requestFocus();
							} else {
								EditText t2 = (EditText)findViewById(getResources().getIdentifier(nexNum2, "id", getPackageName()));
								if(t2 != null){
									t2.requestFocus();
								}
							}
						}
					}
				}
			});
		}
		return editText;
	}

	// rac 추가 버튼
	private ImageButton createDeleteButton(final RelativeLayout newUpper, final LinearLayout marginT, final RelativeLayout.LayoutParams racParams9) {

		final ImageButton newDelBtn = new ImageButton(this);
		newDelBtn.setImageDrawable(getResources().getDrawable(R.drawable.icon_del));
		newDelBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.border2));

		newDelBtn.setId(Integer.parseInt(racCount.get("cnt")) + 1 + 100);
		newDelBtn.setLayoutParams(racParams9);
		newDelBtn.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View vv){

				racArea.removeView(newUpper);
				racArea.removeView(marginT);

				for(int z=0; z<resultRacs.size(); z++) {
					HashMap<String, String> delinfo = resultRacs.get(z);
					int delTxtId = Integer.parseInt(delinfo.get("txtId"));
					if(newDelBtn.getId() == delTxtId) {
						resultRacs.remove(z);
						break;
					}
				}
				for(int y=0; y<resultRacs.size(); y++) {
					HashMap<String, String> upTxt = resultRacs.get(y);
					TextView newTxt = (TextView)findViewById(getResources().getIdentifier(String.valueOf(Integer.parseInt(upTxt.get("txtId"))), "id", getPackageName()));
					newTxt.setText("  R" + (y + 1));
				}
			}

		});

		return newDelBtn;
	}
}