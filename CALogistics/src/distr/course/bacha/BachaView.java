package distr.course.bacha;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import ca.apps.logistics.R;
import distr.common.commons;
import distr.course.CourseList;
import distr.main.MenuActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class BachaView extends Activity {

	// title bar
	Button home;

	// Gisa Info
	private EditText day_edit;
	private EditText name_edit;
	private EditText carNum_edit;
	private EditText start_edit;
	private EditText desti_edit;

	// Rac Info
	private LinearLayout racArea;
	private Button racAdd;
	final ArrayList<HashMap<String, String>> resultRacs = new ArrayList<HashMap<String, String>>();
	final HashMap<String, String> racCount = new HashMap<String, String>();
	final HashMap<String, String> editIdCnt = new HashMap<String, String>();

	// Etc Info
	private EditText etc_edit1;
	private EditText etc_edit2;
	private EditText etc_edit3;
	private EditText etc_edit4;
	private EditText etc_edit5;

	private Button confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_bachaview);

		Toast.makeText(BachaView.this, "승인 완료된 배차입니다.", Toast.LENGTH_SHORT).show();

		// 공통 dp ( 사용할 DIP )
		final float scale = getResources().getDisplayMetrics().density;
		int dp_10 = (int) (10 * scale);
		int dp_20 = (int) (20 * scale);
		int dp_30 = (int) (30 * scale);
		final int dp_33 = (int) (33 * scale);
		int dp_35 = (int) (35 * scale);
		final int dp_45 = (int) (83 * scale);
		final int dp_85 = (int) (123 * scale);
		final int dp_125 = (int) (163 * scale);
		final int dp_165 = (int) (203 * scale);
		final int dp_205 = (int) (243 * scale);
		final int dp_245 = (int) (283 * scale);
		final int dp_285 = (int) (323 * scale);

		// 필요한 값 가져오기 gisa_info, rac_info, etc_info, login_info
		Intent intent = getIntent();
		final HashMap<String, String> gisaInfo = (HashMap<String, String>) intent.getSerializableExtra("gisaInfo");
		final ArrayList<HashMap<String, String>> racInfo = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("racInfo");
		final ArrayList<HashMap<String, String>> etcInfo = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("etcInfo");
		final HashMap<String, String> user = (HashMap<String, String>) intent.getSerializableExtra("user");

		// title bar start
		home = (Button) findViewById(R.id.home);

		home.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){
				Intent homeIntent = new Intent(BachaView.this, MenuActivity.class);
				homeIntent.putExtra("user", user);
				startActivityForResult(homeIntent, 0);
			}
		});
		// title bar end

		// gisa info end
		day_edit = (EditText) findViewById(R.id.dayEdit);
		name_edit = (EditText) findViewById(R.id.nameEdit);
		carNum_edit = (EditText) findViewById(R.id.carNumEdit);
		start_edit = (EditText)findViewById(R.id.startEdit);
		desti_edit = (EditText)findViewById(R.id.destiEdit);

		day_edit.setText(gisaInfo.get("bc_dt"));
		name_edit.setText(gisaInfo.get("gs_nm"));
		carNum_edit.setText(gisaInfo.get("cl_no"));
		start_edit.setText(gisaInfo.get("plc_st_nm"));
		desti_edit.setText(gisaInfo.get("plc_en_nm"));
		// gisa info end

		// rac Info 초기 설정 start
		racArea = (LinearLayout) findViewById(R.id.racArea);
		racAdd = (Button) findViewById(R.id.racAdd);
		final RelativeLayout.LayoutParams racParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams3 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams4 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams5 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams6 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams7 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams8 = new RelativeLayout.LayoutParams(dp_35, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final RelativeLayout.LayoutParams racParams9 = new RelativeLayout.LayoutParams(dp_35, dp_35);
		final LinearLayout.LayoutParams marginTparam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

		if ( racInfo != null) {

			int racSize = racInfo.size();
			int idValue=200;

			for(int idxx=0; idxx<racSize; idxx++) {

				// rac textView, editView 일련 번호 처리
				HashMap<String, String> resuRac = new HashMap<String, String>();
				HashMap<String, String> racs = racInfo.get(idxx);
				resuRac.put("txtId", String.valueOf((idxx + 1) + 100));
				resuRac.put("editId", String.valueOf(idValue));
				racCount.put("cnt", String.valueOf(idxx + 1));
				resultRacs.add(resuRac);
				resuRac = new HashMap<String, String>();

				// rac layout 생성
				final RelativeLayout upper = new RelativeLayout(BachaView.this);
				upper.setLayoutParams(racParams);

				// gap layout 생성
				marginTparam.topMargin = dp_10;
				final LinearLayout marginT = new LinearLayout(BachaView.this);
				marginT.setLayoutParams(marginTparam);

				// rac Text 생성
				final TextView txt = new TextView(this);
				txt.setId((idxx + 1) + 100);
				txt.setText("  R" + (idxx + 1));
				txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_bullet, 0, 0, 0);
				txt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
				racParams2.addRule(RelativeLayout.CENTER_VERTICAL);
				txt.setLayoutParams(racParams2);

				upper.addView(txt);

				// rac edit 생성
				for(int dx=0; dx<6;dx++) {

					EditText edit = new EditText(this);
					edit.setId((idxx+1) + (dx+1) + idValue);
					edit.setEnabled(false);
					edit.setText(racs.get("rc_cd").substring(dx, dx + 1));
					edit.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
					edit.setEms(1);
					edit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
					edit.setBackgroundDrawable(getResources().getDrawable(R.drawable.border3));
					edit.setGravity(Gravity.CENTER);

					if( dx==0) {
						racParams3.leftMargin = dp_45;
						edit.setLayoutParams(racParams3);
					} else if( dx==1) {
						racParams4.leftMargin = dp_85;
						edit.setLayoutParams(racParams4);
					} else if( dx==2) {
						racParams5.leftMargin = dp_125;
						edit.setLayoutParams(racParams5);
					} else if( dx==3) {
						racParams6.leftMargin = dp_165;
						edit.setLayoutParams(racParams6);
					} else if( dx==4) {
						racParams7.leftMargin = dp_205;
						edit.setLayoutParams(racParams7);
					} else {
						racParams8.leftMargin = dp_245;
						edit.setLayoutParams(racParams8);
					}
					upper.addView(edit);

				}

				editIdCnt.put("editId", String.valueOf(idValue));
				idValue += 100;

				racParams9.leftMargin = dp_285;

				upper.setId(idxx+77);

				racArea.addView(upper);
				racArea.addView(marginT);
			}

		}
		// rac Info 초기 설정 end

		// etc info start
		etc_edit1 = (EditText) findViewById(R.id.etcEdit1);
		etc_edit2 = (EditText) findViewById(R.id.etcEdit2);
		etc_edit3 = (EditText) findViewById(R.id.etcEdit3);
		etc_edit4 = (EditText) findViewById(R.id.etcEdit4);
		etc_edit5 = (EditText) findViewById(R.id.etcEdit5);

		// comma process
		final DecimalFormat df = new DecimalFormat("###,###,###,###");

		if (etcInfo != null){

			int listMax = etcInfo.size();
			for (int idx=0; idx<listMax; idx++) {
				HashMap<String, String> map = etcInfo.get(idx);
				String bsp = map.get("bsp_ccd");
				String cnt = df.format(map.get("bsp_cnt"));

				if(bsp.equals("BSPCCD_01")) {
					etc_edit1.setText(cnt);

				} else if(bsp.equals("BSPCCD_02")) {
					etc_edit2.setText(cnt);

				} else if(bsp.equals("BSPCCD_03")){
					etc_edit3.setText(cnt);

				} else if(bsp.equals("BSPCCD_04")){
					etc_edit4.setText(cnt);
				}
			}
		}

		etc_edit5.setText(gisaInfo.get("gj_dc"));
		// etc info end

		confirm = (Button) findViewById(R.id.confirm);

		confirm.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				commons comm = new commons();
				String url = distr.common.commons.ServerUrl + "/elms/baecha/ajaxList2.do";

				ArrayList<HashMap<String, String>> elList = (ArrayList<HashMap<String, String>>) comm.SendByHttp3(url, "gs_cd", user.get("gs_cd"));

				Intent backIntent = new Intent(BachaView.this, CourseList.class);
				backIntent.putExtra("user", user);
				backIntent.putExtra("elList", elList);
				startActivityForResult(backIntent, 0);
			}
		});
	}
}