package distr.course;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import ca.apps.logistics.R;
import distr.common.commons;
import distr.course.bacha.BachaInfo;
import distr.course.bacha.BachaView;
import distr.main.MenuActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class CourseList extends Activity {

	Button home; // title
	ArrayList<course> arItem;
	ArrayList<course> arItem2;
	ArrayList<course> arItem3;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_today);

		Intent intent = getIntent();
		final ArrayList<HashMap<String, String>> elList = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("elList");
		final HashMap<String, String> user = (HashMap<String, String>) intent.getSerializableExtra("user");

		home = (Button) findViewById(R.id.home);
		home.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){
				Intent homeIntent = new Intent(CourseList.this, MenuActivity.class);
				homeIntent.putExtra("user", user);
				startActivityForResult(homeIntent, 0);
			}
		});

		arItem = new ArrayList<course>();
		// 클래스의 객체를 선언하여
		course mi;
		// 객체를 생성하여 arItem에 담음
		for(int i=0; i<elList.size(); i++) {
			HashMap<String, String> courseInfo = elList.get(i);
			String depName = "";

			if(i > 0 ) {
				depName = elList.get(i-1).get("plc_en_nm");
			} else {
				depName = courseInfo.get("plc_st_nm");
			}
			String arrName = courseInfo.get("plc_en_nm");

			if(courseInfo.get("plc_st_nm") != null){
				mi = new course((i + 1), depName, arrName, courseInfo, user);
				arItem.add(mi);
			}
		}
		// BaseAdpater를 상속받아 선언
		// 리스트 뷰에게 이 항목의 집합을 제공하는 클래스
		// 어댑터를 동작하려면 BaseAdapter로부터 상속받아
		// 기본 기능을 물려받은 후 요구하는 추상 메서드를 재정의 해야함.
		MyListAdapters MyAdapter = new MyListAdapters(this, R.layout.course_item2, arItem);

		ListView MyList;
		MyList=(ListView)findViewById(R.id.courseList);
		MyList.setAdapter(MyAdapter);
	}
}

//리스트 뷰에 출력할 항목
class course {
	course(int acnt, String adepName, String aarrName, HashMap<String, String> acourseInfo, HashMap<String, String> auser) {
		depName = adepName;
		arrName = aarrName;
		courseInfo = acourseInfo;
		userInfo = auser;
		cnt = acnt;
	}
	int cnt;
	String depName;
	String arrName;
	HashMap<String, String> courseInfo;
	HashMap<String, String> userInfo;
}

//어댑터 클래스
class MyListAdapters extends BaseAdapter {
	Context maincon;
	LayoutInflater Inflater;
	ArrayList<course> arSrc;
	int layout;

	public MyListAdapters(Context context, int alayout, ArrayList<course> aarSrc) {
		maincon = context;
		Inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arSrc = aarSrc;
		layout = alayout;
	}

	public int getCount() {
		return arSrc.size();
	}

	public String getItem(int position) {
		return arSrc.get(position).depName;
	}

	public long getItemId(int position) {
		return position;
	}

	// 항목 하나를 출력하기 위한 뷰
	// 각 항목의 뷰 생성
	// position -> 생성할 항목의 순서값
	// parent -> 생성되는 뷰의 부모
	// convertView -> 이전에 생성된 차일드 뷰
	public View getView(int position, View convertView, ViewGroup parent) {
		final int pos = position;
		if (convertView == null) {
			convertView = Inflater.inflate(layout, parent, false);
		}

		final HashMap<String, String> user = arSrc.get(pos).userInfo;
		final HashMap<String, String> courseDetail = arSrc.get(pos).courseInfo;

		final float scale = maincon.getResources().getDisplayMetrics().density;
		int padding_13dp = (int) (13 * scale + 0.5f);
		int padding_15dp = (int) (15 * scale + 0.5f);
		int padding_25dp = (int) (25 * scale + 0.5f);

		TextView btn = (TextView)convertView.findViewById(R.id.smsTxt);

		if(courseDetail.get("uh_gb").equals("BCSTGB_03")) {
			btn.setBackgroundDrawable(maincon.getResources().getDrawable(R.drawable.btn_select3));
		} else if(courseDetail.get("uh_gb").equals("BCSTGB_04")) {
			btn.setBackgroundDrawable(maincon.getResources().getDrawable(R.drawable.btn_select4));
		} else {
			btn.setBackgroundDrawable(maincon.getResources().getDrawable(R.drawable.btn_select2));
		}
		btn.setPadding(padding_25dp, padding_13dp, padding_15dp, padding_13dp);
		btn.setText(arSrc.get(pos).cnt + ".  " + arSrc.get(pos).depName + "    →    " + arSrc.get(pos).arrName + "   ");
		btn.setOnClickListener(new Button.OnClickListener(){

			public void onClick(View v){

				// gisa Info
				commons comm = new commons();

				// rac info
				String urls = distr.common.commons.ServerUrl + "/elms/bcrack/ajaxList.do";
				ArrayList<HashMap<String, String>> racList = (ArrayList<HashMap<String, String>>) comm.SendByHttp3(urls, "bcuh_cd", courseDetail.get("bcuh_cd"));

				// etc Info
				String url = distr.common.commons.ServerUrl + "/elms/bcbusokpumsu/ajaxList.do";
				ArrayList<HashMap<String, String>> elList = (ArrayList<HashMap<String, String>>) comm.SendByHttp3(url, "bcuh_cd", courseDetail.get("bcuh_cd"));

				if(courseDetail.get("uh_gb").equals("BCSTGB_04")) {
					Intent nextIntent = new Intent(maincon, BachaView.class);
					nextIntent.putExtra("gisaInfo", courseDetail);
					nextIntent.putExtra("racInfo", racList);
					nextIntent.putExtra("etcInfo", elList);
					nextIntent.putExtra("user", user);
					maincon.startActivity(nextIntent);
				} else {
					Intent nextIntent = new Intent(maincon, BachaInfo.class);
					nextIntent.putExtra("gisaInfo", courseDetail);
					nextIntent.putExtra("racInfo", racList);
					nextIntent.putExtra("etcInfo", elList);
					nextIntent.putExtra("user", user);
					maincon.startActivity(nextIntent);
				}
			}
		});

		return convertView;
	}
}