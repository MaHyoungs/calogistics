package distr.course;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import ca.apps.logistics.R;
import distr.common.commons;
import distr.main.MenuActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class CourseAlarmDetail extends Activity{

	// title bar
	Button home;

	private TextView dateTxt;
	private Button list;
	private WebView contentsView;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_alarm_detail);

		Intent intent = getIntent();
		final HashMap<String, String> user = (HashMap<String, String>) intent.getSerializableExtra("user");
		final HashMap<String, String> alarms = (HashMap<String, String>) intent.getSerializableExtra("alarms");

		// title bar start
		home = (Button) findViewById(R.id.home);

		home.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){
				Intent homeIntent = new Intent(CourseAlarmDetail.this, MenuActivity.class);
				homeIntent.putExtra("user", user);
				startActivityForResult(homeIntent, 0);
			}
		});
		// title bar end

		dateTxt = (TextView) findViewById(R.id.dateTxt);
		contentsView = (WebView) findViewById(R.id.contentsView);
		list = (Button) findViewById(R.id.list);

		String bcdate = alarms.get("subject");
		String SendDate = alarms.get("send_date").substring(0,4) + "." + alarms.get("send_date").substring(4,6) + "." + alarms.get("send_date").substring(6,8) + ".";

		if(bcdate.contains("일")){
			bcdate = bcdate.substring(0, bcdate.indexOf("일"));
		}

		String tag =
					"<span style=\"font-size:17px;color:#212121;\">" + user.get("gs_nm") + "</span>\t\n" +
					"<span style=\"color:#353535;\"> 기사님.</span>\t\n" +
					"   </br>\n" +
					"   </br>\n" +
					"<span style=\"color:#888888;\">" + alarms.get("subject") + "</span>\n" +
					"   </br>\n" +
					"   </br>\n" +
					"   </br>\n" +
					"   </br>\n" +
					"<pre>" +
					"                           <span style=\"color:#8888888;\">" + SendDate + "</span></br>\n" +
					"                           <span style=\"color:#8888888;\">" + alarms.get("sms_msg").substring((alarms.get("sms_msg").indexOf("-") + 1), alarms.get("sms_msg").lastIndexOf("-")) + "</span>" +
					"</pre>";

		contentsView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		contentsView.setVerticalScrollBarEnabled(false);
		contentsView.getSettings().setLoadsImagesAutomatically(true);
		dateTxt.setText("  " + bcdate + "일 배차 알림");
		contentsView.loadData(tag, "text/html; charset=utf-8", "UTF-8");
		contentsView.setWebViewClient(new contentsViewClient());
		list.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){

				commons comm = new commons();
				String url = distr.common.commons.ServerUrl + "/elms/sms/ajaxList2.do";
				ArrayList<HashMap<String, String>> alarmList = (ArrayList<HashMap<String, String>>) comm.SendByHttp2(url, "dest_name", user.get("gs_nm").replaceAll("-", ""), "phone_number", user.get("gs_tel").replaceAll("-", ""));

				if(alarmList == null){
					Toast.makeText(CourseAlarmDetail.this, "배차알림 정보가 없습니다.", Toast.LENGTH_SHORT).show();
				}else{
					Intent newIntent = new Intent(CourseAlarmDetail.this, CourseAlarm.class);
					newIntent.putExtra("user", user);
					newIntent.putExtra("alarmList", alarmList);
					startActivityForResult(newIntent, 0);
				}
			}
		});
	}
	private class contentsViewClient extends WebViewClient{

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
}
