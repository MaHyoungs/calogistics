package distr.course;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ca.apps.logistics.R;
import distr.common.commons;
import distr.main.MenuActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class CourseAlarm extends Activity{

	Button home; // title
	ArrayList<MyItem> arItem;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_alarm);

		Intent intent = getIntent();
		final HashMap<String, String> user = (HashMap<String, String>) intent.getSerializableExtra("user");
		final ArrayList<HashMap<String, String>> alarmList = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("alarmList");

		home = (Button) findViewById(R.id.home);
		home.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v){
				Intent homeIntent = new Intent(CourseAlarm.this, MenuActivity.class);
				homeIntent.putExtra("user", user);
				startActivityForResult(homeIntent, 0);
			}
		});

		arItem = new ArrayList<MyItem>();
		// 클래스의 객체를 선언하여
		MyItem mi;
		// 객체를 생성하여 arItem에 담음

		for(int i=0; i<alarmList.size(); i++) {
			HashMap<String, String> alarmInfo = alarmList.get(i);
			// 배차 알림 = title, bc_dt
			String title = alarmInfo.get("sms_msg").substring(0, alarmInfo.get("sms_msg").indexOf(","));
			String bcdate = alarmInfo.get("subject");

			if(bcdate.contains("일")){
				bcdate = bcdate.replace("년 ", "/");
				bcdate = bcdate.replace("월 ", "/");
				bcdate = bcdate.substring(0, bcdate.indexOf("일"));
			}

			mi = new MyItem(title, bcdate, alarmInfo, user);
			arItem.add(mi);
		}
		// BaseAdpater를 상속받아 선언
		// 리스트 뷰에게 이 항목의 집합을 제공하는 클래스
		// 어댑터를 동작하려면 BaseAdapter로부터 상속받아
		// 기본 기능을 물려받은 후 요구하는 추상 메서드를 재정의 해야함.
		MyListAdapter MyAdapter = new MyListAdapter(this, R.layout.course_item, arItem);

		ListView MyList;
		MyList=(ListView)findViewById(R.id.alarmList);
		MyList.setAdapter(MyAdapter);
	}
}

//리스트 뷰에 출력할 항목
class MyItem {
	MyItem(String atitle, String abcdate, HashMap<String, String> aalarmInfo, HashMap<String, String> auser) {
		title = atitle;
		bcdate = abcdate;
		alarmInfo = aalarmInfo;
		userInfo = auser;
	}
	String title;
	String bcdate;
	HashMap<String, String> alarmInfo;
	HashMap<String, String> userInfo;
}

//어댑터 클래스
class MyListAdapter extends BaseAdapter {
	Context maincon;
	LayoutInflater Inflater;
	ArrayList<MyItem> arSrc;
	int layout;

	public MyListAdapter(Context context, int alayout, ArrayList<MyItem> aarSrc) {
		maincon = context;
		Inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		arSrc = aarSrc;
		layout = alayout;
	}

	public int getCount() {
		return arSrc.size();
	}

	public String getItem(int position) {
		return arSrc.get(position).title;
	}

	public long getItemId(int position) {
		return position;
	}

	// 항목 하나를 출력하기 위한 뷰
	// 각 항목의 뷰 생성
	// position -> 생성할 항목의 순서값
	// parent -> 생성되는 뷰의 부모
	// convertView -> 이전에 생성된 차일드 뷰
	public View getView(int position, View convertView, ViewGroup parent) {
		final int pos = position;
		if (convertView == null) {
			convertView = Inflater.inflate(layout, parent, false);
		}

		final HashMap<String, String> user = arSrc.get(pos).userInfo;
		final HashMap<String, String> alarmDetail = arSrc.get(pos).alarmInfo;
		final String Date = arSrc.get(pos).bcdate;

		TextView btn = (TextView)convertView.findViewById(R.id.smsTxt);
		btn.setText(arSrc.get(pos).title);
		btn.setOnClickListener(new Button.OnClickListener() {

			public void onClick(View v) {

				commons comm = new commons();

				String chckUrl = commons.ServerUrl + "/elms/baecha/ajaxConfirmCheck.do";
				ArrayList<HashMap<String, String>> mapList = (ArrayList<HashMap<String, String>>) comm.SendByHttp2(chckUrl, "gs_cd", user.get("gs_cd"), "bc_dt", Date);

				if(mapList != null) {
					String logUrl = commons.ServerUrl + "/elms/baecha/ajaxConfirmAlaram.do";
					comm.SendByHttp(logUrl, "gs_cd", user.get("gs_cd"), "bc_dt", Date);
				}

				if(alarmDetail.get("send_date") == null) {
					Toast.makeText(maincon, "배차 상세 내역이 존재하지 않습니다.", Toast.LENGTH_SHORT).show();
				} else {
					Intent nextIntent = new Intent(maincon, CourseAlarmDetail.class);
					nextIntent.putExtra("user", user);
					nextIntent.putExtra("alarms", alarmDetail);
					maincon.startActivity(nextIntent);
				}
			}
		});

		return convertView;
	}
}