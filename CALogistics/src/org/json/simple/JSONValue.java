/*
 * $Id: JSONValue.java,v 1.1 2006/04/15 14:37:04 platform Exp $
 * Created on 2006-4-15
 */
package org.json.simple;

import java.io.Reader;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.JSONParser;

/**
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONValue {

	/**
	 * parse into java object from input source.
	 * @param in
	 * @return instance of : JSONObject,JSONArray,String,Boolean,Long,Double or null
	 */
	public static Object parse(Reader in) {
		try {
			JSONParser parser = new JSONParser();
			return parser.parse(in);
		}
		catch (Exception e) {
			return null;
		}
	}

	public static String toJSONString(Object value) {
		if (value == null)
			return "null";

		if (value instanceof String)
			return "\"" + escape((String) value) + "\"";

		if (value instanceof Double) {
			if (((Double) value).isInfinite() || ((Double) value).isNaN())
				return "null";
			else
				return value.toString();
		}

		if (value instanceof Float) {
			if (((Float) value).isInfinite() || ((Float) value).isNaN())
				return "null";
			else
				return value.toString();
		}

		if (value instanceof Number)
			return value.toString();

		if (value instanceof Boolean)
			return value.toString();

		if (value instanceof Map)
			return JSONObject.toJSONString((Map) value);

		if (value instanceof List)
			return JSONArray.toJSONString((List) value);

		return value.toString();
	}

	public static String escape(String s) {
		return escape(s, new StringBuffer());
	}

	public static String escape(String s, StringBuffer sb) {
		if (s == null)
			return null;
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			switch (ch) {
				case '"':
					sb.append("\\\"");
					break;
				case '\\':
					sb.append("\\\\");
					break;
				case '\b':
					sb.append("\\b");
					break;
				case '\f':
					sb.append("\\f");
					break;
				case '\n':
					sb.append("\\n");
					break;
				case '\r':
					sb.append("\\r");
					break;
				case '\t':
					sb.append("\\t");
					break;
				case '/':
					sb.append("\\/");
					break;
				default:
					if (ch >= '\u0000' && ch <= '\u001F') {
						String ss = Integer.toHexString(ch);
						sb.append("\\u");
						for (int k = 0; k < 4 - ss.length(); k++) {
							sb.append('0');
						}
						sb.append(ss.toUpperCase());
					}
					else {
						sb.append(ch);
					}
			}
		}// for
		return sb.toString();
	}
}
