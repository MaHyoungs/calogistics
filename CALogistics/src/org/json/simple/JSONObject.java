/*
 * $Id: JSONObject.java,v 1.1 2006/04/15 14:10:48 platform Exp $
 * Created on 2006-4-10
 */
package org.json.simple;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONObject extends HashMap {

	private static final long serialVersionUID = -513213523753362081L;

	public String toString() {
		return toJSONString();
	}

	public static String toString(String key, Object value) {
		StringBuffer sb = new StringBuffer();

		sb.append("\"");
		sb.append(JSONValue.escape(key));
		sb.append("\":");
		if (value == null) {
			sb.append("null");
			return sb.toString();
		}

		if (value instanceof String) {
			sb.append("\"");
			sb.append(JSONValue.escape((String) value));
			sb.append("\"");
		}
		else
			sb.append(value);
		return sb.toString();
	}

	public static String toJSONString(Map map) {
		if (map == null)
			return "null";

		StringBuffer sb = new StringBuffer();
		boolean first = true;
		Iterator iter = map.entrySet().iterator();

		sb.append('{');
		while (iter.hasNext()) {
			if (first)
				first = false;
			else
				sb.append(',');

			Entry entry = (Entry) iter.next();
			toJSONString(String.valueOf(entry.getKey()), entry.getValue(), sb);
		}
		sb.append('}');
		return sb.toString();
	}

	public String toJSONString() {
		return JSONValue.toJSONString(this);
	}

	private static String toJSONString(String key, Object value, StringBuffer sb) {
		sb.append('\"');
		if (key == null)
			sb.append("null");
		else
			JSONValue.escape(key, sb);
		sb.append('\"').append(':');

		sb.append(JSONValue.toJSONString(value));

		return sb.toString();
	}

	public static String toQueryString(Map map) {
		if (map == null)
			return "";

		StringBuffer sb = new StringBuffer();
		boolean first = true;
		Iterator iter = map.entrySet().iterator();

		while (iter.hasNext()) {
			if (first)
				first = false;
			else
				sb.append('&');

			Entry entry = (Entry) iter.next();
			toQueryString(String.valueOf(entry.getKey()), entry.getValue(), sb);
		}
		return sb.toString();
	}

	public String toQueryString() {
		return toQueryString(this);
	}

	private static String toQueryString(String key, Object value, StringBuffer sb) {
		if (key == null)
			sb.append("");
		else
			JSONValue.escape(key, sb);
		sb.append('=');

		sb.append(value);

		return sb.toString();
	}

	public JSONObject put(String key, boolean value) {
		put(key, value ? Boolean.TRUE : Boolean.FALSE);
		return this;
	}

	public JSONObject put(String key, int value) {
		super.put(key, new Integer(value));
		return this;
	}

	// boolean이 없어서 삭제
	// public boolean getBoolean(String key) {
	// Object o = get(key);
	// if (o.equals(Boolean.FALSE) || (o instanceof String && ((String) o).equalsIgnoreCase("false"))) {
	// return false;
	// }
	// else if (o.equals(Boolean.TRUE) || (o instanceof String && ((String) o).equalsIgnoreCase("true"))) {
	// return true;
	// }
	// return false;
	// }

	public int getInt(String key) {
		Object o = get(key);
		try {
			return o instanceof Number ? ((Number) o).intValue() : Integer.parseInt((String) o);
		}
		catch (Exception e) {
		}
		return 0;
	}

	public String getString(String key) {
		return get(key).toString();
	}

	public char getChar(String key) {
		return getString(key).charAt(0);
	}

}
