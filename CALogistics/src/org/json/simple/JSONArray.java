/*
 * $Id: JSONArray.java,v 1.1 2006/04/15 14:10:48 platform Exp $
 * Created on 2006-4-10
 */
package org.json.simple;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONArray extends ArrayList {

	private static final long serialVersionUID = -1169794020178055911L;

	public static String toJSONString(List list) {
		if (list == null)
			return "null";

		boolean first = true;
		StringBuffer sb = new StringBuffer();
		Iterator iter = list.iterator();

		sb.append('[');
		while (iter.hasNext()) {
			if (first)
				first = false;
			else
				sb.append(',');

			Object value = iter.next();
			if (value == null) {
				sb.append("null");
				continue;
			}
			sb.append(JSONValue.toJSONString(value));
		}
		sb.append(']');
		return sb.toString();
	}

	public String toJSONString() {
		return toJSONString(this);
	}

	public String toString() {
		return toJSONString();
	}

}
